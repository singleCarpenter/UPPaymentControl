
Pod::Spec.new do |s|

   s.name         = 'UPPaymentControl'

   s.version      = '3.3.14'

   s.summary      = '中国银联手机支付初步自动化接入'

   s.homepage     = 'https://open.unionpay.com'

   s.authors      = '中国银联'
   
   s.source       = { :git => 'https://gitee.com/singleCarpenter/UPPaymentControl.git' }

   s.platform      = :ios, "8.0"

   s.source_files = 'UPPaymentControl/*.h'

   s.vendored_libraries = 'UPPaymentControl/libPaymentControl.a'

   s.frameworks   = 'CFNetwork', 'SystemConfiguration'

   s.requires_arc = true

   s.libraries    = 'z'

end


